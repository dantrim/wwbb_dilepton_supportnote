%-------------------------------------------------------------------------------
% This file provides a skeleton ATLAS document.
%-------------------------------------------------------------------------------
% \pdfoutput=1
% The \pdfoutput command is needed by arXiv/JHEP/JINST to ensure use of pdflatex.
% It should be included in the first 5 lines of the file.
%-------------------------------------------------------------------------------
% Specify where ATLAS LaTeX style files can be found.
\newcommand*{\ATLASLATEXPATH}{latex/}
% Use this variant if the files are in a central location, e.g. $HOME/texmf.
% \newcommand*{\ATLASLATEXPATH}{}
%-------------------------------------------------------------------------------

\documentclass[UKenglish,texlive=2013]{\ATLASLATEXPATH atlasdoc}

\makeatletter
\DeclareOldFontCommand{\rm}{\normalfont\rmfamily}{\mathrm}
\DeclareOldFontCommand{\sf}{\normalfont\sffamily}{\mathsf}
\DeclareOldFontCommand{\tt}{\normalfont\ttfamily}{\mathtt}
\DeclareOldFontCommand{\bf}{\normalfont\bfseries}{\mathbf}
\DeclareOldFontCommand{\it}{\normalfont\itshape}{\mathit}
\DeclareOldFontCommand{\sl}{\normalfont\slshape}{\@nomath\sl}
\DeclareOldFontCommand{\sc}{\normalfont\scshape}{\@nomath\sc}
\makeatother

% The language of the document must be set: usually UKenglish or USenglish.
% british and american also work!
% Commonly used options:
%  texlive=YYYY          Specify TeX Live version (2013 is default).
%  atlasstyle=true|false Use ATLAS style for document (default).
%  coverpage             Create ATLAS draft cover page for collaboration circulation.
%                        See atlas-draft-cover.tex for a list of variables that should be defined.
%  cernpreprint          Create front page for a CERN preprint.
%                        See atlas-preprint-cover.tex for a list of variables that should be defined.
%  PAPER                 The document is an ATLAS paper (draft).
%  CONF                  The document is a CONF note (draft).
%  PUB                   The document is a PUB note (draft).
%  txfonts=true|false    Use txfonts rather than the default newtx - needed for arXiv submission.
%  paper=a4|letter       Set paper size to A4 (default) or letter.

%-------------------------------------------------------------------------------
% Extra packages:
\usepackage{\ATLASLATEXPATH atlaspackage}
% Commonly used options:
%  biblatex=true|false   Use biblatex (default) or bibtex for the bibliography.
%  backend=biber         Use the biber backend rather than bibtex.
%  subfigure|subfig|subcaption  to use one of these packages for figures in figures.
%  minimal               Minimal set of packages.
%  default               Standard set of packages.
%  full                  Full set of packages.
%-------------------------------------------------------------------------------
% Style file with biblatex options for ATLAS documents.
\usepackage{\ATLASLATEXPATH atlasbiblatex}

% Package for creating list of authors and contributors to the analysis.
\usepackage{\ATLASLATEXPATH atlascontribute}

% Useful macros
\usepackage[BSM=true]{\ATLASLATEXPATH atlasphysics}
% See doc/atlas-physics.pdf for a list of the defined symbols.
% Default options are:
%   true:  journal, misc, particle, unit, xref
%   false: BSM, hion, math, process, other, texmf
% See the package for details on the options.

% Files with references for use with biblatex.
% Note that biber gives an error if it finds empty bib files.
\addbibresource{int-note.bib}
\addbibresource{HHWWbb1L.bib}
\addbibresource{HHWWbb1LPaper.bib}
\addbibresource{bibtex/bib/ATLAS.bib}
\addbibresource{bibtex/bib/ConfNotes.bib}

% Paths for figures - do not forget the / at the end of the directory name.
\graphicspath{{logos/}{figures/}}

% Add you own definitions here (file atlas-document-defs.sty).
\usepackage{int-note-defs}
\usepackage{adjustbox}


%-------------------------------------------------------------------------------
% Generic document information
%-------------------------------------------------------------------------------

% Title, abstract and document 
\include{int-note-metadata}
% Author and title for the PDF file
%\hypersetup{pdftitle={ATLAS document},pdfauthor={The ATLAS Collaboration}}


\usepackage{hyperref}
\usepackage{arydshln} % for all the lolz
\usepackage{listings} % write code format
\lstset{frame=tb,
  language=C++,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=none,
  numberstyle=\tiny\color{orange},
  keywordstyle=\color{red},
  commentstyle=\color{gray},
  stringstyle=\color{mauve},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3
}

%-------------------------------------------------------------------------------
% Content
%-------------------------------------------------------------------------------

%---------------------------------
% slash notation
%---------------------------------
\usepackage{slashed}
\newcommand*{\vecMET}{\ensuremath{\vec{E}_T^{\,miss}}\xspace}
\newcommand*{\myMET}{\ensuremath{\vec{\slashed{E}}_T}\xspace}
\newcommand*{\Zn}{\ensuremath{\texttt{RooStats::NumberCountingUtils::BinomialExpZ}}\xspace}

%---------------------------------
% define variables
\newcommand*{\HTRatio}{\ensuremath{H_{T2}^{R}}\xspace}
\newcommand*{\mttwo}{\ensuremath{m_{T2}}\xspace}
\newcommand*{\mtllbb}{\ensuremath{m_{T2}^{\ell \ell bb}}\xspace}
\newcommand*{\mtbb}{\ensuremath{m_{T2}^{bb}}\xspace}
\newcommand*{\mbb}{\ensuremath{m_{bb}}\xspace}
\newcommand*{\dRll}{\ensuremath{\Delta R_{\ell \ell}}\xspace}
\newcommand*{\MTscaled}{\ensuremath{M_{T}^{\text{scaled}}}\xspace}
\newcommand*{\MT}{\ensuremath{M_{T}}\xspace}
\newcommand*{\wwbb}{\ensuremath{hh \rightarrow WWbb}\xspace}

%---------------------------------
% include only working section
%---------------------------------
\includeonly{sections/backgroundestimation}

\begin{document}

\maketitle

\tableofcontents
\clearpage
% remove these for now
%\listoffigures
%\clearpage
%\listoftables
%\clearpage

%-------------------------------------------------------------------------------
% Print the list of contributors to the analysis
% The argument gives the fraction of the text width used for the names
%-------------------------------------------------------------------------------
\PrintAtlasContribute{0.30}
\clearpage

%___________________________________________________________
% introduction
\input{sections/introduction}
%___________________________________________________________

%___________________________________________________________
% samples
% discussion on MC (bkg + sig) samples used as well as the 
% dataset (+GRL)
\input{sections/samples}
%___________________________________________________________

%___________________________________________________________
% object definitions
% discussion of object definitions
\input{sections/object_definitions}
%___________________________________________________________

%___________________________________________________________
% selection and analysis strategy 
\input{sections/selection_and_strategy}
%___________________________________________________________

%___________________________________________________________
% background estimation
\input{sections/background_estimation}
%___________________________________________________________

%___________________________________________________________
% systematic uncertainties
\input{sections/systematics}
%___________________________________________________________

\include{sections/blah}

%%-------------------------------------------------------------------------------
%% Missing
%\input{sections/missingitems}
%\clearpage
%%-------------------------------------------------------------------------------

%%%-------------------------------------------------------------------------------
%% Updates
%\input{sections/updates}
%\clearpage
%%%-------------------------------------------------------------------------------
%
%%-------------------------------------------------------------------------------
%% Introduction
%\input{sections/introduction}
%%-------------------------------------------------------------------------------

%%-------------------------------------------------------------------------------
\clearpage
\appendix
\part*{Appendix}
\addcontentsline{toc}{part}{Appendix}

\input{sections/appendix_wwbb_interference_uncertainties}
%%-------------------------------------------------------------------------------

%-------------------------------------------------------------------------------
% If you use biblatex and either biber or bibtex to process the bibliography
% just say \printbibliography here
\printbibliography
% If you want to use the traditional BibTeX you need to use the syntax below.
%\bibliographystyle{bibtex/bst/atlasBibStyleWoTitle}
%\bibliography{atlas-document,bibtex/bib/ATLAS}
%-------------------------------------------------------------------------------

\end{document}
