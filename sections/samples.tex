\section{Data and Simulation}
\label{sec:samples}

In this section we describe the dataset collected by the ATLAS detector as well as the
simulated Monte Carlo samples used, both for Standard Model background estimation \& prediction and
for signal simulation.


\subsection{Data}

The analysis presented here uses the data samples collected by the ATLAS detector
during the periods June-November 2015 and April-October 2016. The data used in the
analysis is required to have all relevant components of the ATLAS detector in nominal
operating condition. The data satisfying this requirement is specified by the
good-runs-list (GRL) prepared by ATLAS data preparation. The GRL derived for the
2015 and 2016 data taking periods used in the present analysis, as well as the associated
amount of data that each accounts for, is presented in Table \ref{tab:grl20152016}.
In total, for both data taking periods, there is $36.1 \ifb$ of integrated luminosity.

\begin{table}[h!]
    %\begin{center}
    \hspace{-1cm}
    \begin{tabular}{| c | c | c |}
    \hline
    Year & GRL & $\int \mathcal{L} dt$ [pb] \\
    \hline
    2015 & {\texttt{data15\_13TeV.periodAllYear\_DetStatus-v79-repro20-02\_DQDefects-00-02-02}} & \\
         & \hspace{6cm}{\texttt{\_PHYS\_StandardGRL\_All\_Good\_25ns}} & 3212.96 \\
    \hline
    2016 & {\texttt{data16\_13TeV.periodAllYear\_DetStatus-v88-pro20-21\_DQDefects-00-02-04}} & \\
         & \hspace{6cm}{\texttt{\_PHYS\_StandardGRL\_All\_Good\_25ns}} & 32861.6 \\
    \hline
    \end{tabular}
    %\end{center}
    \caption{\label{tab:grl20152016} The 2015 and 2016 good-runs-lists (GRLs) used in the analysis.}
\end{table}

\subsection{Monte Carlo Samples}

Below we describe the Monte Carlo (MC) samples used in the analysis. All MC samples for background
processes are passed through the full \textsc{GEANT4}~\cite{Agostinelli:2002hh} simulation of the
ATLAS detector~\cite{Aad:2010ah} whereas the signal samples are passed through the ATLAS Fast Simulation.
All simulated hits are processed and reconstructed with the same software as used for data.

Additionally, the effect of multiple $pp$ interactions in the same and neighbouring
bunch crossings (pile-up) was included by overlaying minimum-bias
collisions, simulated with {\sc Pythia}\,8.186~\cite{Sjostrand:2008c},
on each generated signal and background event. The number of overlaid
collisions was such that the distribution of the number of interactions
per $pp$ bunch crossing in the simulation matches that observed in
the data: on average 14 interactions per bunch crossing in 2015 and
25 interactions per bunch crossing in 2016. This is illustrated in Fig. \ref{fig:avg_mu_2015_2016}.
\footnote{\href{https://twiki.cern.ch/twiki/bin/view/AtlasPublic/LuminosityPublicResultsRun2}{https://twiki.cern.ch/twiki/bin/view/AtlasPublic/LuminosityPublicResultsRun2}}

\begin{center}
    \begin{figure}[!htb]
    \centering
    \hspace{-0.2cm}
    \includegraphics[scale=0.35]{figures/misc/avg_mu_2015_2016}
    \caption{\label{fig:avg_mu_2015_2016} Distribution of the average number of interactions per bunch crossing,
    $<\mu>$, in ATLAS for the 2015+2016 data taking period.}
    \end{figure}
\end{center}


\subsubsection{Standard Model Background Processes}

\subsubsection{Signal Processes}

Signal samples are generated with \MADGRAPH5\_aMC@NLO~\cite{Alwall:2014hca} interfaced to Herwig++ according to
the procedure defined in ~\cite{CP3Paper}. Events are generated with an effective Lagrangian in the
infinite top-quark mass approximation, and reweighting the generated events with form factors that take into
account the finite mass of the top quark. This procedure partially accounts for the finite top-quark mass
effects~\cite{Degrassi_Ramona}.

A re-weighting scheme based on full NLO calculations taking better account of the finite top-mass and correcting
the $m_{hh}$ shape, as described
in the single-lepton $hh \rightarrow WWbb$ and $hh \rightarrow 4b$ analysis, is not applied in the current
analysis. This re-weighting method is described in the meeting {\footnote{\href{https://indico.cern.ch/event/652372/}{https://indico.cern.ch/event/652372/}}}.
Additonal studies will have to be performed in order to understand the effect that such a re-weighting would
have on the present analysis' sensitivity to $hh$ processes.

Table \ref{tab:hh_signals} shows the list of $hh$ signals. They use a heavy Higgs scalar model as the
signal hypotheses, with masses of the heavy Higgs ranging from 260 GeV to 1 TeV while the Higgs
width is set to 10 MeV (Narrow Width Approximation (NWA)). In the present analysis,
the non-resonant signal is normalized to $\sigma (pp \rightarrow hh) \times \text{BR}(hh \rightarrow WWbb) = 0.590$ pb,
whereas the resonant signal hypotheses are all normalized to $44$ fb. 

{\color{red}{check that all signal yields reported use 0.590 pb rather than Yellow Report xsec}}

\begin{table}[!htb]
    \hspace{-1.5cm}
    \begin{scriptsize}
    \begin{tabular}{| c | l | c | c | c | c | r |}
    \hline
    DSID & Process & Generator & $\sigma \times \text{BR}$ [pb] & $k$-factor & $\epsilon_{\text{filter}}$ & N events generated \\
    \hline
    342053 & SM $hh$ (non-resonant), $h\rightarrow hh^* \rightarrow WW^*bb$ & \MADGRAPH5\_aMC@NLO+Herwig++ & 0.590 & 1.0 & 1.0 & \\
    343764 & $H \rightarrow hh \rightarrow WW^*bb$, $m_H = 260$ GeV & \MADGRAPH5\_aMC@NLO+Herwig++ & 0.044& 1.0 & 1.0 \\
    343766 & $H \rightarrow hh \rightarrow WW^*bb$, $m_H = 300$ GeV & \MADGRAPH5\_aMC@NLO+Herwig++ & 0.044& 1.0 & 1.0 \\
    343769 & $H \rightarrow hh \rightarrow WW^*bb$, $m_H = 400$ GeV & \MADGRAPH5\_aMC@NLO+Herwig++ & 0.044& 1.0 & 1.0 \\
    343771 & $H \rightarrow hh \rightarrow WW^*bb$, $m_H = 500$ GeV & \MADGRAPH5\_aMC@NLO+Herwig++ & 0.044& 1.0 & 1.0 \\
    343772 & $H \rightarrow hh \rightarrow WW^*bb$, $m_H = 600$ GeV & \MADGRAPH5\_aMC@NLO+Herwig++ & 0.044& 1.0 & 1.0 \\
    343773 & $H \rightarrow hh \rightarrow WW^*bb$, $m_H = 700$ GeV & \MADGRAPH5\_aMC@NLO+Herwig++ & 0.044& 1.0 & 1.0 \\
    343774 & $H \rightarrow hh \rightarrow WW^*bb$, $m_H = 750$ GeV & \MADGRAPH5\_aMC@NLO+Herwig++ & 0.044& 1.0 & 1.0 \\
    343775 & $H \rightarrow hh \rightarrow WW^*bb$, $m_H = 800$ GeV & \MADGRAPH5\_aMC@NLO+Herwig++ & 0.044& 1.0 & 1.0 \\
    343776 & $H \rightarrow hh \rightarrow WW^*bb$, $m_H = 900$ GeV & \MADGRAPH5\_aMC@NLO+Herwig++ & 0.044& 1.0 & 1.0 \\
    343777 & $H \rightarrow hh \rightarrow WW^*bb$, $m_H = 1000$ GeV & \MADGRAPH5\_aMC@NLO+Herwig++ & 0.044& 1.0 & 1.0 \\
    \hline
    \end{tabular}
    \end{scriptsize}
    \caption{\label{tab:hh_signals} di-Higgs signal samples used in the analysis.}
\end{table}
