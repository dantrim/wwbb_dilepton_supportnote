\section{Uncertainties on the Modelling of the Standard Model Processes}
\label{sec:systematics_modelling}

There are several sources of uncertainty related to the modelling of the dominant
and sub-dominant background processes. We discuss them in turn in what follows.

\subsection{\ttbar Modelling Uncertainties}

The dominant background in our signal regions is \ttbar production. The overall
normalization of the \ttbar process in the SR is constrained by data in its dedicated CR,
however the {\it{shape}} of the \ttbar process is taken from the MC events populating
the SR. Due to differences in shape between the \ttbar templates within the CR, wherein
the SR normalization is determined, and the SR there will be uncertainties related to
this extrapolation, namely theoretical uncertainties which may affect the \ttbar
acceptance in the CR and/or SR, thus affecting its predicted contribution to the SR yields. As a result,
the modelling uncertainties for this process are estimated by compariing the transfer
factors ($TF$s) from the CR to the SR. The $TF$ is defined as:


\begin{align*}
    TF_{CR \rightarrow SR} = \frac{N_{SR}^{MC}}{N_{CR}^{MC}}
\end{align*}
where $N_X^{MC}$ is the number of events for the MC process of interest (here, \ttbar), in MC, predicted in region $X$.

A given source of uncertainty quantified through the $TF$ is then given by the difference,

\begin{align}
    \Delta TF = \frac{| TF_A - TF_B | }{ TF_A }
    \label{eqn:tf_uncertainty_formula}
\end{align}

where $TF_A$ is the $TF$ derived using the nominal sample for which we are deriving the uncertainty,
and $TF_B$ is the alternative sample that we are using to calculate the two-point systematic
uncertainty.

The total modelling uncertainty consists of the following sources:

\begin{itemize}

    \item {\bf{Hard-scatter generation:}} Compared the Powheg+Herwigpp sample (410004) with the
    aMC\@NLO+Herwigpp sample (410003). As neither set of samples in the comparison is the nominal sample,
    the $TF$ uncertainty is calculated twice, changing which set goes in the denominator, and the largest
    uncertainty of the two is taken and symmetrized. As a second
    option, the nominal Powheg+Pythia sample (410009) is also compaired to Sherpa v2.1
    dileptonic \ttbar sample (410189).

    \item {\bf{Fragmentation/Hadronization mode:}} Compared the nominal Powheg+Pythia sample
    (410009) with the Powheg+Herwigpp sample (410004). The final uncertainty is symmetrized.

    \item {\bf{Additional radiation:}} Compared the nominal Powheg+Pythia sample (410009) with
    the radiation variation samples ( {\texttt{radLo}} = 410002, {\texttt{radHi}} = 410001) in which
    the shower radiation, factorization and renormalization, and NLO radiation are varied. The
    largest deviation w.r.t. the nominal from either sample is taken and symmetrized.

\end{itemize}

The uncertainties thus described characterize shifts in acceptance, and so all samples used
for the calculation of the $TF$ uncertainties are done using the truth samples.\footnote{We use {\texttt{TRUTH3}} format for all truth-level studies.}

Compared to the reco-level requirements for the control and signal regions, a slightly looser
selection is applied to increase the statistics at truth-level, given that several of the
alternative/systematic samples can have significantly fewer raw events than the nominal one. The
SR used for the $TF$ uncertainty calculations is simply taken to be the truth-level
equivalent\footnote{For truth-level $b$-jets we use {\texttt{Antikt4TruthJets}} with {\texttt{PartonTruthLabelID}} $==5$}
of the $hh$ selection selection defined in Section \ref{subsec:signal_selection}
and in Table \ref{tab:hh_preselection}. The CR and SR definitions used for the $TF$ uncertainty
calculations are shown in Table \ref{tab:tf_uncertainty_regions}. 

The relevant values used for the calculation of the $TF$s are found in Table \ref{tab:tf_ttbar_yields} and the 
breakdown of the uncertainties, as well as the total modelling uncertainty for the \ttbar process in the SR,
is given in Table \ref{tab:tf_ttbar_uncertainties}.

\begin{table}
    \begin{scriptsize}
    \begin{center}
    \begin{tabular}{ | l | c | c | c | c |}
    \hline
     & {\bf{Truth-level \ttbar CR}} & {\bf{Truth-level $Wt$ CR}} & {\bf{Truth-level SR}} & {\bf{Truth-level SR ($Wt$ Interf.)}}\\
    \hline
    Lepton \pt's [GeV] & $>(20,20)$ & $>(20,20)$ & $>(20,20)$ & $>(20,20)$ \\
    $b$-jet multiplicity & $==2$ & $==2$ & $==2$ & $==2$ \\
    \mbb [GeV] & $\in [100,140]$ & $>140$ & $\in [100,140]$ & $<140$ \\
    \mtllbb [GeV] & $\in [100,140]$ & $-$ & $\in [100,140]$ & $-$ \\
    \dRll & $\in [1.5, 3.0]$ & $-$ & $<0.9$ & $<1.5$ \\
    \HTRatio & $\in [0.4, 0.6]$ & $\in [0.4, 0.8]$ &  $>0.8$ & $>0.8$ \\
    \mtbb & $-$ & $>80$ & $-$ & $>80$ \\
    \hline
    \end{tabular}
    \caption{ \label{tab:tf_uncertainty_regions} Regions used in the $TF$ uncertainty calculations. The truth-level
    SR is common between the $TF$ uncertainty calculations for \ttbar and $Wt$ processes and is based on the
    $hh$ selection defined in \ref{tab:hh_preselection}. For comparison the reco-level
    \ttbar CR is defined in \ref{tab:ttbar_crvr_def} and that of $Wt$ is defined in \ref{tab:wt_crvr_def}.
    The right-most region is used as the SR in the calculation of the $WWbb$ interference related 
    uncertainties which are applied on the $Wt$ process in the analysis' SR (see Appendix \ref{app:wwbb_interference}
    for further details).
    }
    \end{center}
    \end{scriptsize}
\end{table}


\begin{table}
    \begin{center}
    \begin{scriptsize}
    %\hspace{-1.2cm}
    \begin{tabular}{ | l | c | c | c | c | c |}
    \hline
    \hline
    Variation Sample & DSID & {\bf{SR}} & {\bf{CR}} & $TF$ & $\%$ Diff. w.r.t Nominal \\
    \hline
    Powheg+Pythia ({\texttt{Nominal}})  & 410009 & $3131.49 \pm 15.50$ & $16353.59 \pm 35.37$  & $0.192 \pm 0.001$ & $-$ \\
    \hline
    Powheg+Pythia ({\texttt{radHi}}) & 410001 & $3336.26 \pm 51.36$ & $15845.55 \pm 111.91$ & $0.211 \pm 0.004$ & $+9.9$ \\
    Powheg+Pythia ({\texttt{radLo}}) & 410002 & $3056.51 \pm 50.86$ & $16751.56 \pm 119.01$ & $0.183 \pm 0.003$ & $-5.7$\\
    \hline
    aMC\@NLO+Herwigpp & 410003 & $2740.16\pm66.38$ & $16859.03 \pm 170.58$ & $0.163\pm 0.004$ & $-15.1$ \\
    \hline
    Powheg+Herwigpp & 410004 & $2668.85 \pm 37.85$ & $17665.37 \pm 97.38$ & $0.151 \pm 0.002$ & $-21.4$ \\
    \hline
    Sherpa v2.1 & 410189 & $2962.97 \pm 124.63$ & $17142.15 \pm 314.65$ & $0.173\pm0.008$ & $-9.9$ \\
    \hline
    \hline
    \end{tabular}
    \caption{ \label{tab:tf_ttbar_yields} \ttbar truth-level event yields in the CR \& SR used for
    calculating the transfer factors ($TF$s) (Table \ref{tab:tf_uncertainty_regions}). The integrated luminosity is set to $35\ifb$.
    The $TF$ are given in the second-to-last column
    as well as their relative difference with respect to the nominal sample's $TF$. Only the
    statistical uncertainties of the non-nominal samples are used in the uncertainties of the
    $TF$, even when the nominal sample's yields are being used to calculate the $TF$.
    }
    
    \end{scriptsize}
    \end{center}
\end{table}

\begin{table}
    \begin{center}
    \begin{tabular}{ | l | c | }
    \hline
    \hline
    Source & $TF$ Uncertainty [$\%$] \\
    \hline
    Hard-scatter generation & $9.73 \oplus 4.6 \rightarrow 10.8$\\
    Fragmentation/Hadronisation model & $21.1 \oplus 1.5 \rightarrow 21.2$ \\
    Additional radiation & $9.96 \oplus 1.7 \rightarrow 10.1$ \\
    \hline
    Total & $25.4 \oplus 5.12 \rightarrow 25.8$ \\
    \hline
    \hline
    \end{tabular}
    \caption{ \label{tab:tf_ttbar_uncertainties} Break-down of the total modelling uncertainties
    on the \ttbar background in the analysis' SRs. The first number shows the \% difference
    between the central values of the variation and nominal $TF$s (w.r.t. the nominal sample), the
    second shows the statistical uncertainty on the $TF$ difference, and the third shows
    the quadrature sum of the two previous numbers.
    }
    

    \end{center}
\end{table}

\FloatBarrier

\subsection{$Wt$ Modelling Uncertainties}

Behind \ttbar, the single-top process wherein a $t$-quark is produced in association with a $W$ boson, $Wt$,
is the next dominant Standard Model process for the analysis. As in the case of \ttbar, the overall
normalization of the $Wt$ process in the SR is constsrainted by data in its dedicated CR and so
we resort, mainly, to using the transfer factor approach to get an estimate of the modelling uncertainties
for $Wt$ in the SR.

The CR and SR used for the $TF$ calculations are defined in Table \ref{tab:tf_uncertainty_regions}.

The total modelling uncertainty consists of the following sources:

\begin{itemize}

    \item {\bf{Additional radiation:}} Compare the nominal $Wt$ set (410015+410016) with the radiation
    varied set of samples ( {\texttt{radLo}} = (410100+410102), {\texttt{radHi}} = (410099+410101) ) in
    which the shower radiation, factorization, and renormalization, and NLO radiation are varied. The
    largest deviation w.r.t. the nominal from either set of comparisons is taken and symmetrized.

    \item {\bf{Hard-scatter generation:}} Compare the the Powheg+Herwigpp set (410145+410146) with the
    aMC\@NLO+Herwigpp sample (410164). As neither set of samples in the comparison is the nominal sample,
    the $TF$ uncertainty is calculated twice, changing which set goes in the denominator, and the largest
    uncertainty of the two is taken and symmetrized.

    \item {\bf{Fragmentation/Hadronization:}} Compare the nominal set (410015+410016) with the
    Powheg+Herwigpp set (410145+410146). The final uncertainty is symmetrized.

    \item {\bf{Interference Uncertainties:}} Compare the nominal set (410015+410016) which are $Wt$ samples
    produced under the Diagram Removal (DR) scheme with those produced under the Diagram Subtraction (DS)
    scheme (410064+410165). Additional discussion related to these samples and the determination of the
    $WWbb$ interference-related uncertainties can be found in Appendix \ref{app:wwbb_interference}.

\end{itemize}

The relevant values used for the calculation of the $TF$s are found in Table \ref{tab:tf_wt_yields} and the 
breakdown of the uncertainties, as well as the total modelling uncertainty for the $Wt$ process in the SR,
is given in Table \ref{tab:tf_wt_uncertainties}.

%getting yields for cr_crwt
%process wt_nom   dsid 410015  region cr_crwt   weight 0     xsec 3.777009   sumw 2000000.0 :  1043.12 +/- 8.30
%process wt_radLo   dsid 410099  region cr_crwt   weight 0     xsec 35.859759   sumw 0.350425399582 :  1089.90 +/- 11.75
%process wt_radHi   dsid 410100  region cr_crwt   weight 0     xsec 35.845711   sumw 0.334685512997 :  1036.92 +/- 11.42
%process wt_PowhegHpp   dsid 410145  region cr_crwt   weight 0     xsec 3.7772   sumw 1056284.0 :  875.41 +/- 10.50
%process wt_aMCatNLOHpp   dsid 410164  region cr_crwt   weight 0     xsec 7.42824018   sumw 31154.0 :  1460.42 +/- 127.93
%--------------------------------------------------
%getting yields for sr_hhNonRes
%process wt_nom   dsid 410015  region sr_hhNonRes   weight 0     xsec 3.777009   sumw 2000000.0 :  64.71 +/- 2.07
%process wt_radLo   dsid 410099  region sr_hhNonRes   weight 0     xsec 35.859759   sumw 0.350425399582 :  68.46 +/- 2.94
%process wt_radHi   dsid 410100  region sr_hhNonRes   weight 0     xsec 35.845711   sumw 0.334685512997 :  69.46 +/- 2.95
%process wt_PowhegHpp   dsid 410145  region sr_hhNonRes   weight 0     xsec 3.7772   sumw 1056284.0 :  51.94 +/- 2.55
%process wt_aMCatNLOHpp   dsid 410164  region sr_hhNonRes   weight 0     xsec 7.42824018   sumw 31154.0 :  108.49 +/- 34.41
%----------------------------------------------------------------------
% calculate_transfer_factors
% > weight 0
%---------------------------------------------------------------------------
%transfer factors for (SR/CR) = (sr_hhNonRes/cr_crwt)
%- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
% > weight 0
%   TF wt_nom : 0.0620 +/- 0.0020 --> TF uncertainty : 0.0000, stat unc : 0.0329 --> 0.0329
%   TF wt_radLo : 0.0628 +/- 0.0028 --> TF uncertainty : 0.0125, stat unc : 0.0442 --> 0.0460
%   TF wt_radHi : 0.0670 +/- 0.0029 --> TF uncertainty : 0.0798, stat unc : 0.0439 --> 0.0911
%   TF wt_PowhegHpp : 0.0593 +/- 0.0030 --> TF uncertainty : 0.0436, stat unc : 0.0505 --> 0.0667
%   TF wt_aMCatNLOHpp : 0.0743 +/- 0.0244 --> TF uncertainty : 0.1975, stat unc : 0.3290 --> 0.3838

\begin{table}[!htb]
    \begin{center}
    \begin{scriptsize}
    %\hspace{-1.2cm}
    \begin{tabular}{ | l | c | c | c | c | c |}
    \hline
    \hline
    Variation Sample & DSID & {\bf{SR}} & {\bf{CR}} & $TF$ & $\%$ Diff. w.r.t Nominal \\
    \hline
    Powheg+Pythia ({\texttt{Nominal}})  & 410015+410016 & $64.71 \pm 2.07$ & $1043.12 \pm 8.30$ & $0.062 \pm 0.002$ &  $-$ \\
    \hline
    Powheg+Herwigpp & 410145+410146 & $51.94 \pm 2.55$ & $875.41 \pm 10.50 $                    & $0.059 \pm 0.003$ & $-4.8$\\
    aMC\@NLO+Herwigpp & 410164 & $108.49 \pm 34.41$ & $1460.42 \pm 127.93$                      & $0.074 \pm 0.024$ & $+19.4$\\
    \hline
    Powheg+Pythia ({\texttt{radLo}}) & 410100+410102 & $68.46 \pm 2.94$ & $1089.90 \pm 11.75$   & $0.063 \pm 0.003$ & $+1.6$\\
    Powheg+Pythia ({\texttt{radHi}}) & 410099+410101 & $69.46 \pm 2.95$ & $1036.92 \pm 11.42$   & $0.067 \pm 0.003$ & $+8.1$\\
    \hline
    \hline
    \end{tabular}
    \caption{ \label{tab:tf_wt_yields} $Wt$ truth-level event yields in the CR \& SR used for
    calculating the transfer factors ($TF$s) (Table \ref{tab:tf_uncertainty_regions}). The integrated luminosity is set to $35\ifb$.
    The $TF$ are given in the second-to-last column
    as well as their relative difference with respect to the nominal sample's $TF$. Only the
    statistical uncertainties of the non-nominal samples are used in the uncertainties of the
    $TF$, even when the nominal sample's yields are being used to calculate the $TF$.
    }
    
    \end{scriptsize}
    \end{center}
\end{table}

\begin{table}
    \begin{center}
    \begin{tabular}{ | l | c | }
    \hline
    \hline
    Source & $TF$ Uncertainty [$\%$] \\
    \hline
    Hard-scatter generation &  $25.2 \oplus 8.4 \rightarrow 26.6$ \\
    Fragmentation/Hadronisation model & $4.4 \oplus 5.1 \rightarrow 6.7$ \\
    Additional radiation & $7.98 \oplus 4.4 \rightarrow 9.11$ \\
    $WWbb$ Interference & $9.9 \pm 11 \rightarrow 14.7$ \\
    \hline
    Total (without $WWbb$ interf. uncertainty) & $26.8 \oplus 10.8 \rightarrow 28.9$\\
    \hline
    Total (with $WWbb$ interf.) & $28.6 \oplus 15.4 \rightarrow 32.5$ \\
    \hline
    \end{tabular}
    \caption{ \label{tab:tf_wt_uncertainties} Break-down of the total modelling uncertainties
    on the $Wt$ background in the analysis' SRs. The first number shows the \% difference
    between the central values of the variation and nominal $TF$s (w.r.t. the nominal sample), the
    second shows the statistical uncertainty on the $TF$ difference, and the third shows
    the quadrature sum of the two previous numbers.
    }
    \end{center}
\end{table}

{\color{red}{Describe methods used to determine $WWbb$ interference uncertainties -- reference appendix}}

\FloatBarrier
