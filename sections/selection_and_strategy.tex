\section{Event Selection and Analysis Strategy}
\label{sec:selection_and_strategy}

{\color{red}{discuss variable definitions, specifically mT2, and their motivation}}

{\color{red}{variable correlation and ranking}}

\subsection{Event Topology}
\label{subsec:event_topology}

For higgs pair production, the $WWbb$ decay channel is the channel with the second
highest branching ratio, behind the $bbbb$ decay if one considers the Standard Model
Higgs branching ratios on the left hand side of Fig. \ref{fig:higgs_br}. However, reality hits hard when we
realize that we must divide up the $WWbb$ final state based on the various possible decays
of the $W$ bosons. The leptonic decay of the $W$ has a branching ratio of $\sim10\%$,
meaning that by requiring even one of the $W$ bosons to decay leptonically we suffer
a drop in acceptance. Requring two leptons, even more so as seen in the right hand side
of Fig. \ref{fig:higgs_br}.

It should be noted that the present analysis is also sensitive to the $W\rightarrow \tau \nu$
decay modes, with the $\tau$ leptons decaying leptonically to $e$ or $\mu$. Additionally,
there will perhaps be some potential for overlap with the associated $hh\rightarrow bb\tau \tau$
analysis' selection, in any leptonic channels that analysis employs.

\begin{figure}[!htb]
    \centering
    \includegraphics[scale=0.62]{figures/higgs_br_pie}
    \hbox{\hspace{1.0cm} \includegraphics[scale=0.45]{figures/ww_br_tau_pie}}
    \caption{{\it{Left}}: Branching ratios of the Standard Model Higgs boson.
    {\it{Right}}: Branching ratios of a $WW$ system (``lep-lep'': both $W$'s
    decay leptonically (excluding $\tau$ leptons), ``\{lep,tau\}-tau'': both $W$'s
    decay leptonically where at least one lepton is a $\tau$ lepton,
    ``lep-had'': one $W$ decays hadronically, the other 
    leptonically ($e + \mu + \tau$), ``all-had'': both $W$'s decay hadronically). On the right we
    use the PDG listing for the $W$ boson branching ratios. {\color{red}{fix WWbb leptonic BRs}}}
    \label{fig:higgs_br}
\end{figure}
\FloatBarrier

In only requiring one of the $W$ bosons to decay leptonically, and the other hadronically,
the \MET will be due to a single neutrino only and the entire $h\rightarrow WW$
system can be reconstructed. This, along with the $bb$-system means that
one can reconstruct the entire $hh$ system, along with any potential resonance mass.
It should be noted, however, that a correct reconstruction of the $hh$ system relies
on correctly assigning the jet from the hadronically decaying $W$ boson. There are
two potential combinatoric challenges to this, however: 1) assigning the jet to either $W_1$ or $W_2$,
and, 2) due to the non-fully efficient $b-$tagging and $b-$tagging acceptance,
there are cases when a $b-$jet is mis-tagged
and assigned to the wrong side of the $hh$ decay, further degrading the resolution of
the full $hh$ reconstruction. In scenarios where the lepton from $W_1$ is nearby or
overlapping with one of the jets (or the single large-$R$ jet) from hadronic $W_2$,
there will be further degradation in resolution of the $hh$ system reconstruction. This
latter scenario will be a larger problem for electron channels, where ambiguity arises
due to having to assign calorimeter energy deposits to either the electron or the jet.
Doing so wrongly will result in an incorrect/non-ideal $hh$-system reconstruction. This
problem is reduced for muons as they do not deposit a significant (relatively) amount
of their energy in the calorimeters.

In requiring {\it{both}} $W$ bosons to decay leptonically, the \MET in the event
is due to the presence of two neutrinos, the system is kinematically under-constrained,
and the $h\rightarrow WW$ system cannot a-priori be reconstructed. Additionally, the
signal is characterized in this channel by two leptons, two $b-$jets, and the presence
of some \MET. Object-wise, this is the same final state as SM dileptonic $t\bar{t}$ production,
which we'll see
to be the major contributing background. The decreased $WWbb$ acceptance combined
with the (potentially) irreducible SM $t\bar{t}$ background means that the dileptonic
$hh\rightarrow WWbb$ channel may be more difficult and less effective than the other
possible $WWbb$ decays. 

That being said, there are critical differences between the decay of SM $t\bar{t}$
production and $hh\rightarrow WWbb$. The overall {\it{shape}} of the final state
is substantially different between the two. In the case of SM $t\bar{t}$, the decay
is symmetric, meaning that each leg follows the same decay chain: $t \rightarrow Wb \rightarrow \ell \nu b$.
This is illustrated in the left hand side of Fig. \ref{fig:topo}.
In this case, on $\sim$average the momentum flow of the $\ell + \nu$ system from one side opposes
that of the other side and the two $b-$jets oppose one another. That is, the $WW$
system's momentum flow is $\sim$not correlated with that of the $bb$ system's. In the
case of $hh\rightarrow WWbb$, the $WW$ system must balance the momentum of the $bb$
system and the $\nu + \nu$ system must be co-linear with that of the $\ell + \ell$
system, as illustrated in the right hand side of Fig. \ref{fig:topo}.
This is a striking topological difference that will be taken advantage
of in this analysis. 


\begin{figure}[!htb]
    \centering
    \includegraphics[scale=0.62]{figures/ttbar_topo}
    \includegraphics[scale=0.42]{figures/hh_topo}
    \caption{$WWbb$ shapes. {\it{Left}}: SM $t\bar{t}$ production. {\it{Right}}: 
    $hh\rightarrow WWbb$.}
    \label{fig:topo}
\end{figure}
\FloatBarrier

In Figs. \ref{fig:shapes_bb}-\ref{fig:shapes_metll} we present several plots of kinematic
quantities that illustrate the differing topologies between the expected dominant
backgrounds ($t\bar{t}$ and $Wt$ production), non-resonant $hh$ production, and 
several resonant $hh$ signals of varying resonant masses.

Fig. \ref{fig:shapes_bb} shows distributions related to the $bb$ system. It can be
seen from the $\Delta \phi$ and $\Delta R$ distributions that the two $b-$jets
are co-linear with one another for the signal scenarios, and, in the case of resonant production, becoming
more and more co-linear as the resonant mass increases. Even for non-resonant
$hh$ production it can be seen that the $b-$jets are quite co-linear, almost entirely
contained within the same hemisphere of the event as seen in the $\Delta \phi$ distribution.
For $t\bar{t}$ the tendencies in $\Delta \phi$ and $\Delta R$ are reversed, as
expected from their different topology with the $b-$jets on opposing sides of the
decay. For $Wt$ we see essentially the same as for $t\bar{t}$ but, since the second
$b-$jet in $Wt$ is due to higher oder contributions and/or a mis-tag, the $b-$jets
are often more co-linear since they are not necessarily on the opposite side of the
event (as seen in the $\Delta \phi$ distribution).

Fig. \ref{fig:shapes_metll} shows a few distributions related to the dilepton,
\MET, and \MET + dilepton system. We can see from the $\Delta \phi$ and $\Delta R$
distributions that for the signal scenarios the leptons are co-linear, as expected:
both leptons being contained almost entirely within a cone of radius $\Delta R = 1$.
While for the backgrounds there is a tendency for the leptons to be back-to-back
($\Delta \phi ~(\Delta R) \rightarrow \pi$). In the \MET distribution, as well,
we see the expected behavior that, due to the co-linear neutrinos from the
$WW$ system, the \MET tends to be larger for the signal scenarios as compared to the
SM backgrounds. The $\Delta \phi$ between the \MET and dilepton system shows that the
total $WW$ system is co-linear as well. For $t\bar{t}$ the $\Delta \phi$ between
the \MET and dilepton system tends slowly towards $\pi$ as does the $Wt$ process,
however for the latter there is a tendency to gather at lower values due to the same
reasoning as mentioned in the preceding paragraph in the case of the $\Delta \phi$ between the
two $b-$jets (the more co-linear $b-$jets force the neutrinos/leptons to become more co-linear
to balance them).

\begin{figure}[!htb]
    \centering
    \includegraphics[width=0.47\textwidth]{figures/shape_plots/shape_plot_wwbbpre_dphi_bb}
    \includegraphics[width=0.47\textwidth]{figures/shape_plots/shape_plot_wwbbpre_dRbb}
    \caption{{\it{Left}}: $\Delta \phi$ between the two $b-$jets. {\it{Right}}: 
    $\Delta R$ between the two $b-$jets. The selection applied requires exactly 2 signal leptons,
    exactly 2 $b-$jets, dilepton invariant mass ($m_{\ell \ell}$) $>20$ GeV, and
    leading (sub-leading) lepton \pT $>25$ ($>20$) GeV.} 
    \label{fig:shapes_bb}
\end{figure}

\begin{figure}[!htb]
    \centering
    \includegraphics[width=0.47\textwidth]{figures/shape_plots/shape_plot_wwbbpre_dphi_ll}
    \includegraphics[width=0.47\textwidth]{figures/shape_plots/shape_plot_wwbbpre_dRll}
    \includegraphics[width=0.47\textwidth]{figures/shape_plots/shape_plot_wwbbpre_met}
    \includegraphics[width=0.47\textwidth]{figures/shape_plots/shape_plot_wwbbpre_dphi_met_ll}
    \caption{{\it{Left, upper}}: $\Delta \phi$ between the two leptons. {\it{Right, upper}}:
    $\Delta R$ between the two leptons. {\it{Left, lower}}: \MET. {\it{Right, lower}}:
    $\Delta \phi$ between the \MET and dilepton system. The selection applied requires
    exactly 2 signal leptons, exactly 2 $b-$jets, dilepton invariant mass ($m_{\ell \ell}$)
    $>20$ GeV, and leading (sub-leading) lepton \pT $>25$ ($>20$) GeV.}
    \label{fig:shapes_metll}
\end{figure}
\FloatBarrier

\subsection{Kinematic Variables}
\label{subsec:kinematic_variables}

In this section we present a few additional observables that are used in the analysis
to target $hh$ production and provide separation from the expected dominant SM
backgrounds, $t\bar{t}$ and $Wt$ production.

\paragraph{\HTRatio} \hspace{0pt} \\
\label{sec:ht2ratio}

To take advantage of the topological differences mentioned in the preceding sections,
we can construct an observable that is sensitive to the overall distribution of the
momentum flow in the event in order to capture the more ``planar'' nature of
$hh\rightarrow WWbb$ decay. We define such an observable as in Eq. \ref{eq:ht2ratio_def},

\begin{align}
    %\frac{2}{3}
    \HTRatio = \frac{ | \vecMET + \vec{p}_{T,\ell 0} + \vec{p}_{T,\ell 1} | + | \vec{p}_{T, b0} + \vec{p}_{T, b1} | }{ |\vecMET| + |\vec{p}_{T,\ell 0}| + |\vec{p}_{T,\ell 1}| + |\vec{p}_{T, b0}| + |\vec{p}_{T,b1}| }\hspace{0.5cm},
    \label{eq:ht2ratio_def}
\end{align}
\FloatBarrier

where the subscript $\ell 0$ ($\ell 1$) refer to the lead (sub-lead) lepton and 
the subscript $b0$ ($b1$) refer to the lead (sub-lead) $b-$ jet.

The numerator of
Eq. \ref{eq:ht2ratio_def} contains two parts, each the magnitude of a vectorial sum
of the transverse momenta: the first part contains the objects associated with the
$WW$ decay and the second contains the objects associated
with the $bb$ decay. That is, the numerator is the sum of the magnitudes
of each of the higgs' momenta in the case of $hh$ production. The denominator
is simply the scalar sum of the transverse momenta of each of the objects considered in the analysis.
By taking this ratio, we get a measure of the overall spread of the momentum in the
event.

For the expected dominant SM processes, $t\bar{t}$ and $Wt$, the $b-$jets tend
to be more back-to-back and so the second term in the numerator will be smaller due
to the vectorial cancellation. The same goes for the neutrino and lepton systems, making
the first term in the numerator also smaller for the SM processes. The ratio then
gives the overal relative cancellation of momenta due to the symmetric or asymmetric
topology of the $WWbb$ decay. For $hh$, both terms in the numerator will be large 
(Fig. \ref{fig:shapes_bb} - \ref{fig:shapes_metll}). It follows then that the 
\HTRatio quantity should tend towards unity for the signal scenarios and towards
lower values for the SM processes. This behavior is illustrated in Fig. \ref{fig:shapes_HT2Ratio}.

\begin{figure}[!htb]
    \centering
    \includegraphics[width=0.6\textwidth]{figures/shape_plots/shape_plot_wwbbpre_HT2Ratio}
    \caption{Normalized \HTRatio distribution for SM $t\bar{t}$ and $Wt$ production and
    resonant and non-resonant $hh$ production scenarios. The selection applied requires
    exactly 2 signal leptons, exactly 2 $b-$jets, dilepton invariant mass ($m_{\ell \ell}$)
    $>20$ GeV, and leading (sub-leading) lepton \pT $>25$ ($>20$) GeV.}
    \label{fig:shapes_HT2Ratio}
\end{figure}
\FloatBarrier


We have also found that several transverse-mass-like variables are quite useful in
providing discriminatory power against the SM processes and $hh$ production.

\paragraph{\MT} \hspace{0pt} \\
\label{sec:mt1}

One such transverse-mass variable that we use is simply the transverse mass of the
event, which we call here \MT and define as in Eq. \ref{eq:mt1_def}, 

\begin{align}
    \MT = \sqrt{\left( E_T^{~\text{vis}} + E_T^{\text{~miss}} \right)^2 - \left| \left( \vec{p}_T^{~\text{vis}} + \vec{E}_T^{~\text{miss}} \right) \right|^2 } \hspace{0.5cm},
    \label{eq:mt1_def}
\end{align}
where ``vis'' refers to the visible system, $p_{\ell 0}^{\mu} + p_{\ell 1}^{\mu} + p_{b 0}^{\mu} + p_{b 1}^{\mu} \equiv p_{\ell \ell}^{\mu} + p_{bb}^{\mu}$, and $E_T^{~\text{vis}} = \sqrt{ |\vec{p}_T^{~\text{vis}}|^2 + m_{\text{vis}}^2}$. We also define a
variable \MTscaled, which is defined the same as in Eq. \ref{eq:mt1_def} but before
its calculation, we scale the energy and \pT of the $bb$ system by the ratio
of $125.09/m_{bb} \equiv m_h/m_{bb}$. That is,

\begin{lstlisting}
    //pseudo-code for b-jet system re-scaling
    TLorentzVector bjet_system = ...;
    double mbb = bjet_system.M();
    double scaling = 125.09 / mbb;
    bjet_system.SetPtEtaPhiE(bjet_system.Pt() * scaling, bjet_system.Eta(), bjet_system.Phi(), bjet_system.E() * scaling);
\end{lstlisting}

In Fig. \ref{fig:shapes_MT_1} we show a representative distribution of both \MT and
\MTscaled for $t\bar{t}$, $Wt$, non-resonant $hh$ production, and resonant $hh$
production for several resonant mass hypotheses. We see that the $b-$jet system
re-scaling as described above improves the overall resolution of \MT. For this reason
\MTscaled is found to be a useful observable for improving the sensitivity to 
resonant $hh$ production.

{\color{red}{Quantify amount by which the resolution is improved, and also mass
dependence of this value -- and where the use of the scaling fails us (e.g. parton luminosity effects)}}

\begin{figure}[!htb]
    \centering
    \includegraphics[width=0.48\textwidth]{figures/shape_plots/shape_plot_wwbbpre_MT_1}
    \includegraphics[width=0.48\textwidth]{figures/shape_plots/shape_plot_wwbbpre_MT_1_scaled}
    \caption{Normalized \MT distributions. {\it{Left}}: \MT without the $b-$jet system energy and \pT re-scaling.
    {\it{Right}}: \MT with the $b-$jet system re-scaling. The selection applied
    requires exactly 2 signal leptons, exactly 2 $b-$jets, dilepton invariant mass
    ($m_{\ell \ell}$) $>20$ GeV, and leading (sub-leading) lepton \pT $>25$ ($>20$) GeV.}
    \label{fig:shapes_MT_1}
\end{figure}
\FloatBarrier

We also consider several variants of the \mttwo variable ~\cite{0954-3899-29-10-304, Lester2011, 1126-6708-2008-04-034, PhysRevD.78.034019, PhysRevD.81.031102}.
\mttwo is a transverse-mass variable that can be used in events where there
are two or more invisible particles, as is our case with the two neutrinos. Generally,
\mttwo assumes that the decay is symmetric, as in $t\bar{t}$, where there are two
decay legs, each with a visible and invisible daughter (each of these may be
composite). In the case of $hh$ production, the decay is not symmetric as one leg
follows the $h\rightarrow WW$ decay and the other follows the $h\rightarrow bb$ decay
and has no invisible particles. However, \mttwo can still be constructed in these
asymmetric cases.

\mttwo is defined as in Eq. \ref{eq:mt2_def}.

\begin{align}
    \mttwo^2 \equiv \min_{\slashed{q}_T^a + \slashed{q}_T^b = \slashed{E}_T^{\text{miss}}}
        \left[ \max 
                \left\{ m_T^2(\vec{p}_T^{~\text{vis, a}}, \slashed{q}_T^a),
                        m_T^2(\vec{p}_T^{~\text{vis, b}}, \slashed{q}_T^b) \right\}
        \right] \hspace{0.5cm}
    \label{eq:mt2_def}
\end{align}
where ``a'' and ``b'' correspond to the assumed two sides of the decay and the $p_T^i$
are the transverse momenta of the visible objects on side $i$ and $q_T^i$ is a partition
of the total \MET given to side $i$. The minimum is carried out over all partitionings
of the \MET into the $q_T^i$ and the $m_T$ quantities are the typical transverse mass, as
in Eq. \ref{eq:mt_def}.

\begin{align}
    m_T^2(\vec{p}_T^{~\text{vis}}, \slashed{q}_T) = (E_T^{~\text{vis}} + |\vec{\slashed{q}}_T|)^2 - | ( \vec{p}_T^{~\text{vis}} + \vec{\slashed{q}}_T) |^2
    \label{eq:mt_def}
\end{align}

\paragraph{\mtbb} \hspace{0pt} \\
\label{sec:mt2bb}

The \mtbb variable is an \mttwo construction where the input visible objects are the two
$b-$jets in the event, meaning that side ``a'' contains one of the $b-$jets (plus a 
portion, $\vec{q_a}$, of the \vecMET) and side ``b'' contains the other $b-$jet
(and its $\vec{q_b}$). An illustration of the grouping of the objects used for \mtbb
is given in Fig. \ref{fig:mt2_bb_desc} where the visible side ``a'' objects of \mttwo can be
taken as those circled in blue and the visible side ``b'' objects of \mttwo can be taken
as those circled in red. 

We see in Fig. \ref{fig:mt2_bb_desc} that for the two background processes $t\bar{t}$ and
$Wt$ that the input objects to \mtbb are indeed on opposite sides of the event whereas for
the signal $hh$ process they are on the same side. What's more, for the $t\rightarrow Wb$
legs (two of which exist in the tree-level $t\bar{t}$ process and one in the $Wt$ process),
the $m_T(\text{vis}_i,q_i)$ are kinematically bound to be lower than the mass of the top quark. For
the ``$W$'' side of the $Wt$ process (the side with the $b-$quark circled in {\it{red}} in
the middle of Fig. \ref{fig:mt2_bb_desc}), the $m_T(\text{vis}_i, q_i)$ has no such kinematic bound,
and so the \mtbb quantity for $Wt$ is allowed to attain values larger than $m_{top}$ (and,
therefore, values larger than those attainable by the $t\bar{t}$ process).

For the $hh$ process, the two visible inputs to \mtbb are on the same side of the decay and
oppose that of the \vecMET. Hence, for any combination of $m_T(\text{vis}_i, q_i)$, there is no
kinematic bound. For this reason, we expect the \mtbb quantity to be allowed to attain
large values, perhaps values larger than any of the SM backgrounds.

In Fig. \ref{fig:shape_mt2_bb} we illustrate for several signal hypotheses and the
$t\bar{t}$ and $Wt$ processes the shape of the \mtbb distribution. We see that the
tail of the \mtbb distribution in $Wt$ events extends to larger values than that of
$t\bar{t}$ processes, as expected following our discussion above. We see, too, that for
$hh$ processes the tail extends much further, in general, than any of the SM processes
due to neither \mttwo ``side'' being kinematically bound.

\begin{figure}[!htb]
    \hspace{-1.3cm}
    \centering
    \includegraphics[width=0.26\paperwidth]{figures/mt2_desc/mt2_bb_desc_ttbar}
    \includegraphics[width=0.26\paperwidth]{figures/mt2_desc/mt2_bb_desc_wt}
    \includegraphics[width=0.26\paperwidth]{figures/mt2_desc/mt2_bb_desc_hh}
    \caption{Illustrations of the the object groupings considered for the \mtbb variable
    for a few representative diagrams of the processes of concern.
    {\it{Left}}: $t\bar{t}$, {\it{middle}}: $Wt$, and {\it{right}}: $hh$ production.
    Visible objects are circled with solid lines, invisible objects with dashed lines. The
    red and blue objects are inputs to the \mttwo calculation as the visible objects
    on the separate sides of the decay while the \vecMET is apportioned between them.
    \label{fig:mt2_bb_desc}}
\end{figure}
\FloatBarrier

\begin{figure}[!htb]
    \centering
    \includegraphics[scale=0.55]{figures/shape_plots/shape_plot_wwbbpre_mt2_bb}
    \caption{Normalized \mtbb distribution for SM $t\bar{t}$ and $Wt$ production
    and resonant and non-resonant $hh$ production scenarios. The selection applied
    requires exactly 2 signal leptons, exactly 2 $b-$jets, dilepton invariant mass
    ($m_{\ell \ell}$) $>20$ GeV, and leading (sub-leading) lepton \pT $>25$ ($>20$) GeV.}
    \label{fig:shape_mt2_bb}
\end{figure}
\FloatBarrier

\paragraph{\mtllbb} \hspace{0pt} \\
\label{sec:mt2llbb}

The \mtllbb variable is yet another \mttwo construction. The two visible systems taken
as input to the \mttwo calculation are the dilepton system, $\vec{\ell \ell}$, and the
$b-$jet system, $\vec{bb}$. In this way, the \mttwo side ``a'' can be taken as $\vec{\ell \ell}$
and side ``b'' can be taken as $\vec{bb}$. An illustration of the groupings of the objects
used for \mtllbb is given in Fig. \ref{fig:mt2_llbb_desc} where the visible side ``a'' objects
of the \mttwo calculation can be taken as those circled in blue and the objects taken as
visible side ``b'' in the \mttwo calculation can be taken as those circled in red.

We see in Fig. \ref{fig:mt2_llbb_desc} that for the two background processes $t\bar{t}$ and
$Wt$ that both the $\vec{\ell \ell}$ and $\vec{bb}$ systems are composed of objects that are
on opposing sides of the decay (i.e. each side has one red and one blue object). For this reason,
no matter the splitting of the \vecMET into the $q_i$, the $m_T(\text{vis}_i, q_i)$
are not kinematically bound and are expected to have a wide distribution.

For the $hh$ process, on the other hand, the $\vec{\ell \ell}$ and $\vec{bb}$ systems are composed
of objects that are on the {\it{same}} side of the decay (i.e. both red/blue objects in Fig.
\ref{fig:mt2_llbb_desc} are on the same side). Additionally, the $\vec{bb}$ side does not
contain any true contribution to the \vecMET. For this last reason, there are two types of
\vecMET splittings allowed: one in which both the $\vec{q}_i$ point in the $\vec{\ell \ell}$
direction and one in which only one points in the $\vec{\ell \ell}$ direction. For the former, 
the magnitudes of the $q_i$ will be less than the magnitude of \vecMET. In the latter, in order
to satisfy $\vec{q}_a + \vec{q}_b = \vecMET$, one of the $q_i$ will have magnitude greater than
that of \vecMET. For combinations of these $q_i$ splitting, either the $\vec{bb}$ or $\vec{\ell \ell}$
side can provide maximal $m_T$ in the \mttwo calculation and, further, both will be
kinematically bound by the mass of the Higgs boson, $m_H \sim 125$ GeV. 

In Fig. \ref{fig:shape_mt2llbb} we illustrate for several signal hypotheses and the $t\bar{t}$ and $Wt$
processes the shape of the \mtllbb distribution. We see that for the background processes the distribution
tends to large values and is fairly broad. For the signal processes, however, they are all bound at
$m_H$, and exhibit the typical transverse-mass line shape. These facts follow from our expectations of
the objects that we provide as input to the \mttwo calculation as described above.

\begin{figure}[!htb]
    \centering
    \includegraphics[scale=0.55]{figures/shape_plots/shape_plot_wwbbpre_mt2_llbb}
    \caption{Normalized \mtllbb distribution for SM $t\bar{t}$ and $Wt$ production
    and resonant and non-resonant $hh$ production scenarios. The selection applied
    requires exactly 2 signal leptons, exactly 2 $b-$jets, dilepton invariant mass
    ($m_{\ell \ell}$) $>20$ GeV, and leading (sub-leading) lepton \pT $>25$ ($>20$) GeV.}
    \label{fig:shape_mt2llbb}
\end{figure}
\FloatBarrier

\begin{figure}[!htb]
    \hspace{-1.3cm}
    \centering
    \includegraphics[width=0.26\paperwidth]{figures/mt2_desc/mt2_llbb_desc_ttbar}
    \includegraphics[width=0.26\paperwidth]{figures/mt2_desc/mt2_llbb_desc_wt}
    \includegraphics[width=0.26\paperwidth]{figures/mt2_desc/mt2_llbb_desc_hh}
    \caption{Illustrations of the the object groupings considered for the \mtllbb variable
    for a few representative diagrams of the processes of concern.
    {\it{Left}}: $t\bar{t}$, {\it{middle}}: $Wt$, and {\it{right}}: $hh$ production.
    Visible objects are circled with solid lines, invisible objects with dashed lines. The
    red and blue objects are inputs to the \mttwo calculation as the visible objects
    on the separate sides of the decay while the \vecMET is apportioned between them.
    \label{fig:mt2_llbb_desc}}
\end{figure}
\FloatBarrier

\subsection{Data Selection and Triggers}
\label{subsec:data_selection_and_triggers}

Events are selected using a combination of single lepton and dilepton triggers. Due
to the evolution of the data taking conditions and collision intensity during the
time from the beginning of the 2015 data taking until the end of the 2016 data taking
period, the trigger selection is dependent on the data taking year.

To avoid overcomplication, we consider only the lowest, unprescaled triggers.\footnote{
\href{https://twiki.cern.ch/twiki/bin/view/Atlas/LowestUnprescaled}
{https://twiki.cern.ch/twiki/bin/view/Atlas/LowestUnprescaled}}

The use of a given trigger to determine whether or not to consider the event for
use in the offline analysis is dependent on the flavor of the dilepton event
($ee$, $\mu \mu$, $e\mu$, or $\mu e$) and the \pt of each of the leptons involved.
Additionally, the use of a single lepton trigger over a dilepton trigger depends
on the \pt of the leading lepton in the event. The {\it{set}} of triggers (single 
or dilepton) is determined by the year (2015 or 2016) in which the event was
recorded. For Monte Carlo, where the notion of a "year" is ill-defined, we use
a "random run number" determined based on the luminosity profile of the entire
data sample considered (i.e. the 2015+2016 data sample) such that, on average,
the likelihood of calling a given MC event as being associated with a "year" of 2015
or "2016" follows that of what you would get by polling from events in the data
sample.

The trigger strategy employed is illustrated in Table \ref{tab:trigger_strategy_20152016}. 
For a given year and dilepton flavor, the trigger selection proceeds first by checking
the top-most \pt threshold possibility. If that \pt threshold is satisfied then the
associated trigger item is checked. If that trigger item did not fire, then the process
is repeated for the next \pt threshold and trigger combination until all \pt threshold
possibilites for that dilepton flavor are exhausted. If a \pt threshold is
satisfied and the associated trigger item fires, the event is saved for offline analysis.
If all \pt thresholds fail or none of the triggers fire despite a passed \pt threshold,
the event is not considered for analysis.

Note that in the different flavor events ($e\mu$ or $\mu e$) the assymetric triggers
are additionally used. These enable recovery of acceptance for events with very low-\pt sub-leading
leptons, specifically for the cases where the sub-leading lepton is an electron.

{\color{red}{diagnostic plot: show stability in recorded data over data taking periods}}

{\color{red}{show efficiency for bkg / signals}}

\begin{table}[!htb]
    \hspace{-1.8cm}
    \begin{scriptsize}
    \begin{tabular}{| c |  c | l | c | l |}
    \hline
    & 
    \multicolumn{2}{  c | } { {\bf{2015}} }
    &
    \multicolumn{2}{  c | } { {\bf{2016}} } \\ 
    \hline
    Dilepton Flavor & \pt Threshold(s) & Trigger & \pt Threshold(s) & Trigger \\
    \hline
    $ee$ & $(26, 0)$ & {\texttt{HLT\_e24\_lhmedium\_L1EM20VHI}} & $(28,0)$ & {\texttt{HLT\_e26\_lhtight\_nod0\_ivarloose}} \\
    & $(14,14)$ & {\texttt{HLT\_212\_lhloose\_L12EM10VH}} & $(19,19)$ & {\texttt{HLT\_2e17\_lhvloose\_nod0}} \\
    \hline
    $\mu \mu$  & $(22, 0)$ & {\texttt{HLT\_mu20\_iloose\_L1MU15}} & $(28,0)$ & {\texttt{HLT\_mu28\_ivarmedium}}  \\
               & $(20,10)$ & {\texttt{HLT\_mu18\_mu8noL1}} & $(24,10)$ & {\texttt{HLT\_mu22\_mu8noL1}} \\
    \hline
    $e \mu$ &  $(26,0)$ & {\texttt{HLT\_e24\_lhmedium\_L1EM20VH}} & $(28,0)$ & {\texttt{HLT\_e26\_lhtight\_nod0\_ivarloose}} \\
            &  $(26,10)$ & {\texttt{HLT\_e24\_lhmedium\_nod0\_L1EM20VHI\_mu8noL1}} & $(28,10)$ & {\texttt{HLT\_e26\_lhmedium\_nod0\_L1EM22VHI\_mu8noL1}} \\
            &  $(20,17)$ & {\texttt{HLT\_e17\_lhloose\_mu14}} & $(20,17)$ & {\texttt{HLT\_e17\_lhloose\_mu14}} \\
    \hline
    $\mu e$ &  $(22,0)$ &  {\texttt{HLT\_mu20\_iloose\_L1MU15}} & $(28,0)$ & {\texttt{HLT\_mu26\_ivarmedium}} \\
            &  $(26,10)$ & {\texttt{HLT\_e7\_lhmedium\_mu24}} & $(26,10)$ & {\texttt{HLT\_e7\_lhmedium\_nod0\_mu24}} \\
            &  $(20,17)$ & {\texttt{HLT\_e17\_lhloose\_mu14}} & $(20,17)$ & {\texttt{HLT\_e17\_lhloose\_mu14}}  \\
    \hline
    \end{tabular}
    \caption{ \label{tab:trigger_strategy_20152016} Trigger strategy used for the 2015+2016 analysis. The dilepton
    flavor on the left-most column indicates the \pt ordering of the lepton flavors in the event
    with the higher-\pt lepton on the left (e.g. $e \mu$ indicates a different flavor event where the leading
    lepton is the electron and the sub-leading is the muon).}
    \end{scriptsize}
\end{table}

\subsection{Event Preselection}
\label{subsec:event_preselection}

A number of cleaning cuts are applied in order to ensure that data marked as bad for physics are
not selected:

\begin{itemize}
    \item Data events must have the {\sc larError}, {\sc tileError}, {\sc sctError}, and $(${\sc coreFlags} \& 0x400$)$ flags are
    all false
    \item A primary vertex must be found \footnote{require that in the event's container of vertices that $\exists \ge 1$ with \texttt{xAOD::Vertex->vertexType() == xAOD::VxType::PriVtx}}
        \footnote{ \href{https://twiki.cern.ch/twiki/bin/view/AtlasProtected/TrackingCPMoriond2017}{https://twiki.cern.ch/twiki/bin/view/AtlasProtected/TrackingCPMoriond2017}}
    \item In case a cosmic muon is found in the event, the event is rejected
    \item Events are also rejected if they contain at least one jet with $\pt > 20$ \gev satisfying the {\sc LooseBad}
    criteria defined by the Jet/ETmiss group
    \footnote{\href{https://twiki.cern.ch/twiki/bin/view/AtlasProtected/HowToCleanJets2016}{https://twiki.cern.ch/twiki/bin/view/AtlasProtected/HowToCleanJets2016}}
\end{itemize}

Events that satisfy the above requirements, contain exactly two oppositely-charged signal-level leptons passing the overlap removal,
and satisfy the trigger requirements as described in Table \ref{tab:trigger_strategy_20152016}
are then selected for analysis. We also require that the dilepton invariant mass, $m_{\ell \ell}$, be
greater than $20 \gev$ in order to avoid selecting low-mass resonances: $m_{\ell \ell} > 20$ \gev.


\subsection{Signal Selection}
\label{subsec:signal_selection}

With the events defined by the pre-selection described in the previous section, we
then define regions of phase space that target enhanced signal acceptance. These are our
signal regions (SRs). In the present analysis we consider two separate signal hypotheses:
non-resonant $hh$ production and resonant $hh$ production. For both production scenarios
we define a common "signal pre-selection", which we will call our $hh$ selection.

The $hh$ selection is motivated by the discussion in Section \ref{subsec:event_topology}
and kinematic variables in Section \ref{subsec:kinematic_variables}. In those previous
sections we have seen that there are several observables which share common kinematic features
between both resonant and non-resonant signal hypotheses. With these observables we define
the common $hh$ selection as in Table \ref{tab:hh_preselection}. We also
enforce a $Z$-veto that is 20 \gev wide around the $Z$ pole mass of 91.2 \gev.

We additionally require that the sub-leading lepton \pt in the event be $>20$ \gev. This is
motivated, apart from \pt thresholds described in Table \ref{tab:trigger_strategy_20152016},
by further removing contamination from $Z\rightarrow \tau \tau$ events with leptonically
decaying $\tau$ leptons. Contamination from $Z\rightarrow \ell \ell$ ($\ell$ as $e$ or $\mu$)
are removed sufficiently at this point and are not the main motivating factor behind the
additional requirement on the sub-leading lepton \pt.

%%126 \newcommand*{\HTRatio}{\ensuremath{H_{T2}^{R}}\xspace}
%%127 \newcommand*{\mttwo}{\ensuremath{m_{T2}}\xspace}
%%128 \newcommand*{\mtllbb}{\ensuremath{m_{T2}^{\ell \ell bb}}\xspace}
%%129 \newcommand*{\mtbb}{\ensuremath{m_{T2}^{bb}}\xspace}
%%130 \newcommand*{\mbb}{\ensuremath{m_{bb}}\xspace}
%%131 \newcommand*{\dRll}{\ensuremath{\Delta R_{\ell \ell}}\xspace}
%%132 \newcommand*{\MTscaled}{\ensuremath{M_{T}^{\text{scaled}}}\xspace}
%%133 \newcommand*{\MT}{\ensuremath{M_{T}}\xspace}
%%134 \newcommand*{\wwbb}{\ensuremath{hh \rightarrow WWbb}\xspace}


\begin{table}
    \begin{center}
    \begin{tabular}{| l | c |}
    \hline
    \multicolumn{2}{| c |} {\bf{ $hh$ Signal Selection } } \\
    \hline
    Observable & Selection \\
    \hline
    $m_{\ell \ell}$ [\gev] & (for $ee$ \& $\mu \mu$ events: $|m_{\ell \ell} - 91.2| > 20.0$) \\
    sub-leading lepton \pt [\gev] & $>20$ \\
    $b$-jet multiplicity & $== 2$ \\
    \mbb [\gev] & $\in [100, 140]$ \\
    \mtllbb [\gev] & $\in [100, 140]$ \\
    \dRll & $<0.9$ \\
    \HTRatio & $>0.8$ \\
    \hline
    
    \end{tabular}
    \caption{ \label{tab:hh_preselection} Definition of the $hh$ preselection, common to both
    resonant and non-resonant $hh$ signal region selections. }
    \end{center}

\end{table}
\FloatBarrier

{\color{red}{Add plots showing the purity of signal and background as a function of the $hh$ selection cuts}}

{\color{red}{Add kinematic distributions at the level of $hh$ selection cuts}}

In order to target non-resonant $hh$ selection we are required to reduce the large contamination
of the dominant process $t\bar{t}$ which remains after the $hh$ selection of Table \ref{tab:hh_preselection}.
We do this by requiring events to lie in the tail of the \mtbb distribution (see Fig. \ref{fig:shape_mt2_bb}),
specifically requiring that $\mtbb > 150$ \gev, where the strict kinematic bound in $t\bar{t}$ begins to reduce its
contamination. This selection is described in Table \ref{tab:hh_nonresonant_selection}.

In order to target the resonant $hh$ production hypotheses, we define windows in the transverse mass
observable \MT (Eq. \ref{eq:mt1_def}) that are $\sim$centered on the resonant mass hypotheses (see Figure \ref{fig:shapes_MT_1}).
These selections for the resonance hypotheses considered in the present analysis are described in
Table \ref{tab:hh_resonant_selection}.

\begin{table}
    \begin{center}
    \begin{tabular}{| l | c |}
    \hline
    \multicolumn{2}{|c|} {\bf{ Non-resonant Signal Region }} \\
    \hline
    Observable  & Selection \\
    \hline
    \multicolumn{2}{|c|} { Enforce $hh$ selection, Table \ref{tab:hh_preselection} } \\
    \mtbb [\gev] & $>150$ \\
    \hline
    \end{tabular}
    \caption{ \label{tab:hh_nonresonant_selection} Definition of the signal region targeting
    enhanced non-resonant $hh$ production purity. }
    \end{center}
\end{table}

\begin{table}
    \begin{center}
    \begin{tabular}{| c | c |}
    \hline
    \multicolumn{2}{|c|} { \bf{Resonsant Signal Region(s) }} \\
    \hline
    \multicolumn{2}{|c|} { Enforce $hh$ selection, Table \ref{tab:hh_preselection} } \\
    \hline
    Signal Mass Hypothesis & $\MT$ Window [\gev] \\
    \hline
    260 & $\in [208, 286] $ \\
    300 & $\in [240, 330] $ \\
    400 & $\in [320, 440] $ \\
    500 & $\in [400, 550] $ \\
    600 & $\in [480, 660] $ \\
    700 & $\in [560, 770] $ \\
    750 & $\in [600, 825] $ \\
    800 & $\in [640, 880] $ \\
    900 & $\in [720, 990] $ \\
    1000 & $\in [800, 1100] $ \\
    \hline
    \end{tabular}
    \caption{ \label{tab:hh_resonant_selection} Definition of the \MT (Eq. \ref{eq:mt1_def})
    window selections defining the signal regions targeting sensitivity to various $hh$ resonance hypotheses.}
    \end{center}
\end{table}

\FloatBarrier
