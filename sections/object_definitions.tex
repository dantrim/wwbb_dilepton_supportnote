\section{Object Definitions}
\label{sec:object_definitions}

Here we discuss the selection criteria of the physics objects that are considered
in the analysis. For each object type there are two types of selection: ``baseline''
and ``signal''. The baseline objects are the loosest type that is considered for
a given physics object and are used in the overlap removal procedure to resolve
any ambiguity between the objects that may be considered as signal level objects.
The signal objects are required to pass the baseline definitions by default in addition
to some tighter identification or quality criteria designed to enhance the purity of
the selection. The signal objects are used in the definitions of the signal, control,
and validation regions. The flow of object definition is illustrated in Fig.
\ref{fig:selection_flow}.

\begin{figure}[!htb]
    \centering
    \hbox{\hspace{1.0cm} \includegraphics[width=\textwidth]{figures/selectionPDF} }
    \caption{}
    \label{fig:selection_flow}
\end{figure}

\FloatBarrier

It should be noted that the requirement that the events considered in this analysis
be dilepton events is made using the {\it{signal}} leptons. That is, all events that
do not have exactly two signal leptons are vetoed. No veto on the multiplicity
of baseline leptons is enforced.

%______________________________________________________________________________

\subsection{Leptons}
\label{subsec:lepton_def}

Here we describe the definition of the leptons (namely, electrons and muons) as
they are used in the analysis. Several of the selection and identification criteria
are common between electrons and muons, namely the baseline \pT and isolation requirements.

For both baseline electrons and muons, a requirement that the \pT of the object be 
$>10$ GeV is imposed. There is no additional \pT requirement imposed for the signal
definitions of leptons. The \pT requirement on signal leptons is related to the 
trigger strategy and is considered an analysis-level selection.

The requirement that leptons be isolated is imposed only on {\it{signal}} level
leptons. Baseline leptons are not required to be isolated. The quantities used
to measure the isolation of the leptons are the so-called $p_T^{varcone}$ variables,
which are {\it{track-based}} isolation quantities. For electrons we use the quantity
$p_T^{varcone,0.2}$, defined as the sum of transverse momenta of all tracks,
satisfying quality criteria, within a cone $\Delta R = \min(0.2, 10 ~\text{GeV}/E_T)$
around the considered electron's track ($E_T$, here, is the transverse energy
of the considered electron). For muons, we use the $p_T^{varcone,0.3}$
variable with similar definition as for $p_T^{varcone,0.2}$ but using a 
cone of $\Delta R = \min(0.3, 10 ~\text{GeV}/E_T)$ around the considered muon's track.
The isolation requirement is imposed via the ratio of these isolation quantities to the
\pT of the lepton considered, given by $p_T^{varcone,\{0.2,0.3\}}/\pT < 0.06$.
This isolation selection is implemented within the \texttt{IsolationSelectionTool}
and further details are in the associated Twiki page
\footnote{\href{https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/IsolationSelectionTool}
{https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/IsolationSelectionTool}}. This
isolation selection is referred to as the \texttt{FixedCutTightTrackOnly} isolation
working point within the \texttt{IsolationSelectionTool}.

For all leptons we impose, at the {\it{signal}} level, lepton-to-vertex association
requirements. These requirements are $|d_0^{\text{BL}}~\text{significance}| < 5.0$ ($3.0$) for electrons
(muons),
where $d_0^{\text{BL}}$ is the distance of closest approach of the lepton track in the transverse ($xy$)
plane with respect to the beamline, and $|\Delta z_0^{\text{BL}}\sin \theta| < 0.5$ mm,
where $\Delta z_0^{\text{BL}}$ is the distance of closest aproach
in the longitudinal ($yz$) plane with respect the primary vertex $z$ position and
the beamline and $\theta$ is that of the lepton momenta. These recommendations
and their implementation are further discussed in the Tracking group's Twiki
\footnote{\href{https://twiki.cern.ch/twiki/bin/view/AtlasProtected/TrackingCPMoriond2017}
{https://twiki.cern.ch/twiki/bin/view/AtlasProtected/TrackingCPMoriond2017}}.

%{\color{red}{N.B. These are not the recommended lepton-vertex requirements. For electrons
%it is recommended that $|d_0/\sigma_{d_0}| < 5.0$, though for the current analysis, the
%effect of the difference is negligible. Will change in next iteration of analysis
%n-tuples.}}

\subsubsection{Electrons}
\label{subsec:electron_def}

Electrons are selected from clusters of energy deposits in the calorimeter system that
are matched to single tracks in the inner detector. The electron identification algorithms
are likelihood based and implemented in the \texttt{AsgElectronLikelihoodTool}
and described in ~\cite{Aaboud:2016vfy}.
 We require that baseline and signal electrons
pass the \texttt{LooseLLHBLayer} and \texttt{TightLLH} likelihood identification working points
of the \texttt{AsgElectronLikelihoodTool},
respectively. The \texttt{LooseLLHBLayer} and \texttt{TightLLH} identification WP are $\sim96\%$ and
$\sim 88 \%$ efficient, respectively, for electrons with transverse energy ($E_{\rm T}$) of $100\, \gev$,
where $E_{\rm T}$ is the magnitude of the transverse component of the electron momentum
as measured using the electromagnetic calorimeter.
Further details on the identification of electrons can be found in the
e/gamma Twiki
\footnote{\href{https://twiki.cern.ch/twiki/bin/view/AtlasProtected/EGammaIdentificationRun2}
{https://twiki.cern.ch/twiki/bin/view/AtlasProtected/EGammaIdentificationRun2}}.

The electron cluster is required to be in the range $|\eta^{clus}| < 2.47$ for both
baseline and signal electrons. For signal electrons, we additionally require that the
electron cluster be excluded from the crack region within $1.37 < |\eta^{clus}| < 1.52$.

The summary of the electron definitions can be found in Table \ref{tab:electron_def}.

{\color{red}{TODO: Discuss electron scale factor, calibration, etc.. link to documentation.}}

 
\begin{table}[!htb]
    \centering
    \begin{tabular}{| l | c |}
    \hline
    Selection Quantity & Requirememnt \\
    \hline
    \multicolumn{2}{| c |}{{\bf{Baseline Definition}}} \\
    \hline
    Identification & \texttt{LooseLLHBLayer} \\
    \pT [GeV] & $>10$ \\
    $|\eta^{clus}|$ & $<2.47$ \\
    Quality & \texttt{isGoodOQ(xAOD::EgammaParameters::BADCLUSELECTRON)} \\
    \hline
    \multicolumn{2}{| c |}{{\bf{Signal Definition}}} \\
    \hline
    Identification & \texttt{TightLLH} \\
    Isolation & \texttt{FixedCutTightTrackOnly} \\
    $|d_0^{\text{BL}}\text{significance}|$ & $<5.0$ \\
    $|\Delta z_0^{\text{BL}}\sin \theta|$ [mm] & $<0.5$ \\
    $|\eta^{clus}|$ & $\notin [1.37, 1.52]$ \\
    \hline  
    \end{tabular}
    \caption{Summary of electron definition for baseline and signal electrons used
    in the analysis.}
    \label{tab:electron_def}
\end{table}
\FloatBarrier

%______________________________________________________________________________

\subsubsection{Muons}
\label{subsec:muon_def}

Muons are reconstructed by combining tracks found in the inner-detector and muon
spectrometer whose trajectories and curvatures are consistent~\cite{Aad:2014rra}.
For both baseline and signal muons we require that the muons pass the
\texttt{Medium} WP of the \texttt{MuonSelectionTool}. The reconstruction efficiencies
for muons under the \texttt{Medium} WP are $\sim 96 \%$~\cite{Aad:2016jkr}.

Baseline and signal muons are required to be in the range $|\eta| < 2.4$. 

For a more detailed discussion on muon selection, there is the MCP Twiki page
\footnote{\href{https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MCPAnalysisGuidelinesMC15}
{https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MCPAnalysisGuidelinesMC15}}.

The summary of the muon definitions can be found in Table \ref{tab:muon_def}.

{\color{red}{TODO: discuss corrections,performance, and reference.}}

\begin{table}[!htb]
    \centering
    \begin{tabular}{| l | c |}
    \hline
    Selection Quantity & Requirement \\
    \hline
    \multicolumn{2}{|c|}{{\bf{Baseline Definition}}} \\
    \hline
    Identification & \texttt{Medium} \\
    \pT [GeV] & $>10$ \\
    $|\eta|$ & $<2.4$ \\
    \hline
    \multicolumn{2}{|c|}{{\bf{Signal Definition}}} \\
    \hline
    Identification & \texttt{Medium} \\
    Isolation & \texttt{FixedCutTightTrackOnly} \\
    $|d_0^{\text{BL}}\text{significance}|$ & $<3.0$ \\
    $|\Delta z_0^{\text{BL}}\sin \theta|$ [mm] & $<0.5$ \\
    \hline
    \end{tabular}
    \caption{Summary of muon definition for baseline and signal muons used in the
    analysis.}
    \label{tab:muon_def}
\end{table}
\FloatBarrier

%______________________________________________________________________________

\subsection{Jets}
\label{subsec:jet_def}

The jets used in this analysis are reconstructed from three-dimensional topological
calorimeter clusters ({\it{topo-clusters}}) ~\cite{ATLAS-TopoClustering} using the
anti-$k_t$ clustering algorithm with radius paramter 0.4.~\cite{Cacciari:2008gp}

Baseline jets are required only to have $\pT > 20$ GeV and with no further requirement
on their location within the detector volume.

We differentiate between {\it{signal}} jets and {\it{signal b-jets}}. Signal $b-$jets are
identified using the \texttt{MV2c10} jet flavor
tagging algorithm ~\cite{Aad:2015ydr}. $b-$jets are those identified as having
originated from a $b-$quark or meson using the 85\% WP of the \texttt{MV2c10} 
algorithm. At this WP, the $b$-tagging efficiency is for correctly identifying a $b$-tagged
jet is $85 \%$ for jets with $\pt>20$ \gev, the charm quark rejection is 3.1 and the
light jet rejection is 34. Signal jets identified as a $b-$jet will from now on simply be referred
to as ``b-jets" and those signal jets not identified as a $b-$jet will be reffered
to as either ``signal jets" or ``light-flavor jets".

Signal jets are required to be within the range $|\eta| < 2.8$ whereas $b-$jets
must fall within the tracking volume, $|\eta|<2.5$. 

We use the ``jet vertex tagger" (JVT) ~\cite{ATLAS-CONF-2014-018} quantity to determine
whether a jet arises from a pileup interaction. The criteria used to avoid
selecting a pileup jet as either a signal or $b-$jet is given by JVT $>0.59$ OR
$|\eta|>2.4$ OR $\pT > 60$ GeV. Signal and $b-$jets are required to pass this requrement.

The summary of the jet definitions can be found in Table \ref{tab:jet_def}.

{\color{red}{TODO: discuss correction performance, reference docs.}}


\begin{table}[!htb]
    \centering
    \begin{tabular}{| l | c |}
    \hline
    Selection Quantity & Requirement \\
    \hline
    \multicolumn{2}{|c|}{{\bf{Baseline Definition}}} \\
    \hline
    \pT [GeV] & $>20$ \\
    \hline
    \multicolumn{2}{|c|}{{\bf{Signal Definition}}} \\
    \hline
    $|\eta|$ & $<2.8$ \\
    Pileup suppression & $\texttt{JVT} > 0.59$ || $|\eta| > 2.4$ || $\pT > 60$ GeV \\
    \hline
    \multicolumn{2}{|c|}{{\bf{$b-$Jet Definition}}} \\
    \hline
    $|\eta|$ & $<2.5$ \\
    Pileup suppression & $\texttt{JVT} > 0.59$ || $|\eta| > 2.4$ || $\pT > 60$ GeV \\
    \texttt{MV2c10} &  $> 0.175848$ (85\% $b-$tag efficiency WP) \\
    \hline
    \end{tabular}
    \caption{Summary of baseline, signal, and signal $b-$jet definitions used in
    the analysis.}
    \label{tab:jet_def}
\end{table}
\FloatBarrier

%______________________________________________________________________________

\subsection{Missing Transverse Momentum}
\label{subsec:met_def}

The magnitude of the missing transverse momentum, \MET, is rebuilt using the
calibrated electrons, muons, jets, and photons with all objects at the
baseline level. The track-based soft term (TST) is used for building the total
\MET. Hadronically decaying taus are not explicitly included in the \MET
reconstruction and their contribution is therefore propagated to the soft-term.

The implementation of the \MET reconstruction is provided by the \texttt{METMaker}
tool within the \texttt{METUtilities} package developed by the Jet/ETMiss group.
Further details can be found in the group's Twiki
\footnote{\href{https://twiki.cern.ch/twiki/bin/view/AtlasProtected/EtmissRecommendationsRel20p7}
{https://twiki.cern.ch/twiki/bin/view/AtlasProtected/EtmissRecommendationsRel20p7}}.

%______________________________________________________________________________

\subsection{Overlap Removal}
\label{subsec:overlap_removal}

{\color{red}{show studies on using sliding cone and/or b-jet OR procedure and
inclusion of fake muons}}
{\color{red}{see July 6 UCI informal for OR discussion.}}

The following overlap removal procedure is applied to resolve ambiguities
between objects that are potentially classified in more than one type of physics
object collection. The input objects are those passing the {\it{baseline}}
selection criteria as defined in the previous sub-sections. The procedure
is executed in steps, the order of which is given below. At each step only
those objects that survive the previous step are considered.

\begin{center}
    \begin{enumerate}
    \item {\bf{Electron-Muon Overlap}} : Muon Removal

    If a muon is calo-taged (\texttt{xAOD::Muon::CaloTagged} is {\it{true}})
    and shares an inner-detector track with an electron, remove the muon.

    \item {\bf{Electron-Muon Overlap}} : Electron Removal

    If an electron shares an inner-detector track with a muon, remove the electron.

    \item {\bf{Jet-Electron Overlap}} : Jet Removal

    If an electron and jet overlap within a $\Delta R(\phi, y)$ cone of $0.2$, remove the jet.

    \item {\bf{Jet-Electron Overlap}} : Electron Removal

    If an electron and jet overlap within a $\Delta R(\phi, y)$ cone of $0.4$
    and the jet is not classified as a pileup jet \footnote{To not classify as a
    pileup jet, a jet must satisfy the pileup suppression requirement detailed in
    Sec. \ref{subsec:jet_def}}, remove the electron.

    \item {\bf{Jet-Muon Overlap}} : Jet Removal

    If a muon and a jet overlap within a $\Delta R(\phi, y)$ cone of $0.2$ or
    the muon is ghost-associated to a jet, and the jet does not have $\ge 3$
    associated tracks, remove the jet.

    \item {\bf{Jet-Muon Overlap}} : Muon Removal

    If a muon and a jet overlap within a $\Delta R(\phi, y)$ cone of $0.4$ and
    the jet is not classified as a pileup jet, remove the muon.


    
    \end{enumerate}

\end{center}
