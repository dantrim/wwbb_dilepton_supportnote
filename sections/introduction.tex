\section{Introduction}
\label{sec:introduction}

With the discovery of the Higgs boson by the ATLAS and CMS collaborations in 
2012, one of the last missing pieces of the Standard Model (SM) was found. With the
Higgs boson discovered, we are able to pin down the mechanism by which
Electroweak Symmetry Breaking (EWSB), the lynch pin of the SM, is brought about.
However, now that we have our spin-0 scalar, we need to put it to work and use it
as an avenue towards furthering our understanding. Measurement of its mass and
scalar nature are not necessarily sufficient to provide us with proof that it alone
is the agent of EWSB. After EWSB, the SM predicts precisely what the Higgs potential
should look like:

\begin{align}
V(h) = \frac{1}{2} m_h^2 h^2 + \lambda \nu h^3 + \frac{1}{4} \tilde{\lambda} h^4
\label{eq:higgs_potential}
\end{align}

In Eq. \ref{eq:higgs_potential} the first term represents the kinetic term and the second and third terms
represent the Higgs {\it{self coupling}}. As regards Eq. \ref{eq:higgs_potential}
\footnote{We have of course a rich program of measuring other Higgs properties such
as its branching ratios and couplings to the other particles of the SM},
so far the experiments at the LHC have 
meaningfully probed only the first term, but it is the terms related to the Higgs
self coupling that truly determines the {\it{shape}} of the Higgs potential, and therefore
what determine the underlying nature of EWSB. Indeed, with the vast majority of 
Beyond the Standard Model (BSM) theories requiring additional constraints on the
Electroweak and Higgs sector of the SM, many times extending the Higgs sector
 to include additional {\it{Higgs-like}}
particle content, the LHC program must now work hard to measure the remaining terms of
Eq. \ref{eq:higgs_potential} relating to the Higgs self-coupling. 

The second term of Eq. \ref{eq:higgs_potential} describes vertices with 3 Higgs bosons
and the third term of Eq. \ref{eq:higgs_potential} describes vertices with 4 higgs
bosons and the coupling parameters $\lambda$ and $\tilde{\lambda}$, respectively,
are known as the trilinear and quartic couplings. In the SM we have that
$\tilde{\lambda} = \lambda$. For all intents and purposes the direct 4-Higgs vertex
is not possible to be directly probed at the LHC any time soon but given its relation
to the trilinear coupling it may be probed indirectly. For this reason, focus at the LHC
(and its successor, the HL-LHC) is on the processes which are sensitive to the Higgs trilinear coupling $\lambda$.


The trilinear coupling leads to the non-resonant pair production of Higgs bosons,
where an off-shell Higgs bosons decays to a pair of Higgs bosons, the leading production
mechanism being $gg$ fusion. Direct observation of such Higgs pair production would
mean a direct measurement of $\lambda$ but in the SM there are competing diagrams
that proceed via quark-loops that are sensitive to the Yukawa coupling of the Higgs
rather than the trilinear coupling $\lambda$. These di-Higgs
production mechanisms are illustrated in the Feynman diagrams presented in Fig.
\ref{fig:sm_nonres_hh}. Not only does the quark-loop induced process present itself
as an irreducible background to the Higgs self-coupling sensitive decay, but it
interferes {\it{destructively}} wih the latter, making the observation of this type of
Higgs pair production more challenging. 

\begin{figure}[!htb]
    \centering
    \includegraphics[width=0.9\textwidth]{figures/feynman_diagrams/sm_nonres_hh_production}
    \caption{Representative diagrams that contribute to non-resonant $hh$ production.
    {\it{Left}}: Diagram that is senstive to the $hhh$ coupling. {\it{Right}}: Box
    diagram that interferes destructively with the $hhh$ coupling diagram.}
    \label{fig:sm_nonres_hh}
\end{figure}
 
\FloatBarrier

As a result of this destructive interference, and the relatively large Higgs mass of
125 GeV, the SM Higgs pair production has a total cross-section of $\sim33.4$ fb \cite{CERN-YR-4} at
a $pp$ center of mass collision energy of 13 TeV. The inclusive cross-section for 
the pair production of top quarks, which we'll see to be a dominant background
for the current analysis, is nearly 1000 pb, or $1\times10^6$ fb \cite{TOPQ-2015-09, ATLAS-CONF-2015-049}.
That of {\it{single}} Higgs production is $\sim 50$ pb, or $5 \times 10^4$ fb \cite{ATLAS-CONF-2015-060}.

As a result of its small cross-section, SM di-Higgs production will require a large
dataset before observations at the level of $5 \sigma$ significance can be made or even approached,
datasets that the HL-LHC era physics program will hopefully achieve. There are many BSM
models, however, that predict either extensions of the Higgs sector or new particles
heavy enough and with the right properties to decay to pairs of Higgs bosons. For the
former, the wide class of two Higgs doublet models, or 2HDM, predict an altered (and enlarged)
Higgs sector from which the currently observed Higgs is built.
The Minimal Supersymmetric Standard Model, or MSSM, is a class of 2HDM, for example.
For the latter set, one such model is Randall-Sundrum
type graviton or the lightest Kaluza-Klein excitation which are at least $2\times$ the
mass of the SM Higgs boson. The presence of such BSM
scenarios would act to alter the measured value of $\lambda$ with respect to that of the SM,
potentially enlarging it. As a result, early evidence for the pair production of Higgs
bosons within the current Run-2 datasets may indicate the presence of new physics
without having to resort to precision measurements of $\lambda$.

%any increase in the observed rate of di-Higgs
%production could signal the presence of a wide class of BSM physics scenarios,
%potentially before the measurement of $\lambda$ to high precision. 
%observation of the pair-production of what we now refer to
%as the SM Higgs boson, especially if observed in a resonant channel.

In this note we describe an analysis searching for evidence of both non-resonant
and resonant di-Higgs production, where the di-Higgs system subsequently decays
via the $WWbb$ channel where each of the $W$ bosons decays leptonically.
In Fig. \ref{fig:dilepton_hh_wwbb} we present a representative Feynman diagram
of this decay.

\begin{figure}[!htb]
    \centering
    \includegraphics[width=0.5\textwidth]{figures/feynman_diagrams/hh_wwbb_dilepton}
    \caption{$h = $ SM, $H = $ BSM resonance}
    \label{fig:dilepton_hh_wwbb}
\end{figure}
