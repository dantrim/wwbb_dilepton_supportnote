\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {UKenglish}
\contentsline {section}{\numberline {1}Introduction}{4}{section.1}
\contentsline {section}{\numberline {2}Data and Simulation}{6}{section.2}
\contentsline {section}{\numberline {3}Object Definitions}{6}{section.3}
\contentsline {subsection}{\numberline {3.1}Leptons}{7}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}Electrons}{7}{subsubsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.2}Muons}{8}{subsubsection.3.1.2}
\contentsline {subsection}{\numberline {3.2}Jets}{9}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Missing Transverse Momentum}{9}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Overlap Removal}{10}{subsection.3.4}
\contentsline {section}{\numberline {4}Event Selection and Analysis Strategy}{10}{section.4}
\contentsline {subsection}{\numberline {4.1}Event Topology}{11}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Kinematic Variables}{14}{subsection.4.2}
\contentsline {paragraph}{\nonumberline \ensuremath {H_{T2}^{R}}\xspace }{14}{section*.12}
\contentsline {paragraph}{\nonumberline \ensuremath {M_{T}}\xspace }{16}{section*.14}
\contentsline {paragraph}{\nonumberline \ensuremath {m_{T2}^{bb}}\xspace }{18}{section*.16}
\contentsline {paragraph}{\nonumberline \ensuremath {m_{T2}^{\ell \ell bb}}\xspace }{19}{section*.18}
\contentsline {section}{\numberline {5}Estimation of Standard Model Backgrounds}{20}{section.5}
\contentsline {section}{\numberline {6}Systematic Uncertainties}{20}{section.6}
