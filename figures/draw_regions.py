#!/usr/bin/env python

import matplotlib.pyplot as plt
import matplotlib.patches as patches

class RegionPatch :
    def __init__(self, name = "", color = 'b', alpha = 0.5, hatches = '') :
        self.name = name
        self.x = 0.0
        self.y = 0.0
        self.width = 0.0
        self.height = 0.0
        self.color = color
        self.alpha = alpha
        self.hatches = hatches

    def xy(self) :
        return (self.x, self.y)
    def x(self) :
        return self.x
    def y(self) :
        return self.y
    def wh(self) :
        return (self.width, self.height)
    def w(self) :
        return self.width
    def h(self) :
        return self.height
    def hatches(self) :
        return self.hatches
    def __str__(self) :
        return "RegionPatch %s (x,y)=(%.2f,%.2f), (w,h)=(%.2f,%.2f)" % (self.name, self.x, self.y, self.width, self.height)
    

    def as_rectangle(self) :
        return patches.Rectangle( self.xy(), self.w(), self.h(), facecolor = self.color, alpha = self.alpha, edgecolor = 'k',  fill = True, hatch = self.hatches)

def main() :

 #   import matplotlib.pyplot as plt
 #   import matplotlib.patches as patches
    
    #fig1 = plt.figure()
    #ax1 = fig1.add_subplot(111, aspect='equal')
    #ax1.add_patch(
    #patches.Rectangle(
    #    (0.1, 0.1),   # (x,y)
    #    0.5,          # width
    #    0.5,          # height
    #)
    #)
    #fig1.savefig('rect1.png', dpi=90, bbox_inches='tight')

    print "rectangles"

    #rpcr = RegionPatch("CR$_{t\\bar{t}}$", color = '#ea4335', alpha = 0.8)
    ## x - axis is HT2Ratio
    ## y - axis is dRll
    #rpcr.x = 0.4
    #rpcr.y = 1.5
    #rpcr.width = 0.2
    #rpcr.height = 1.5

    #rpvr = RegionPatch("VR$_{t\\bar{t}}$", color = '#117bf3', alpha = 0.5)
    #rpvr.x = 0.6
    #rpvr.y = 0.9
    #rpvr.width = 0.2
    #rpvr.height = 0.6

    #rpsr = RegionPatch("$hh$", color = "#28f72d", alpha = 0.6)
    #rpsr.x = 0.8
    #rpsr.y = 0
    #rpsr.width = 0.2
    #rpsr.height = 0.9

    #fig = plt.figure()
    #ax = fig.add_subplot(111) #, aspect = 'equal')
    #ax.set_ylim(0, 5)
    #ax.set_xlim(0, 1)
    #ax.grid(color = 'k', linestyle = '--', linewidth = 2, alpha = 0.8, zorder = 0)

    #ax.set_xticks([0.0,0.4,0.6,0.8,1.0])
    #ax.set_yticks([0.0,0.9,1.5,3.0])
    #ax.set_xlabel("H$_{T2,R}$", horizontalalignment = 'right', x = 1, fontsize = 20)
    #ax.set_ylabel("$\\Delta R_{\\ell \\ell}$", horizontalalignment = 'right', y = 1, fontsize = 20)

    #regions = [rpcr, rpvr, rpsr]
    #for reg in regions :
    #    rect = reg.as_rectangle()

    #    ax.add_patch( rect )

    #    ax.add_artist(rect)
    #    rx, ry = rect.get_xy()
    #    cx = rx + rect.get_width()/2.0
    #    cy = ry + rect.get_height()/2.0
    #    ax.annotate(reg.name, (cx, cy), color = 'k', weight = 'bold',
    #        fontsize = 16, ha = 'center', va = 'center')

    #ax.text(0.05, 0.9, "$t\\bar{t}$ Regions", weight = 'bold', transform = ax.transAxes, fontsize = 25, ha = 'left')
    #ax.text(0.05, 0.8, "$m_{bb} \\in [100,140]$", transform = ax.transAxes, fontsize = 20, ha = 'left')
    #ax.text(0.05, 0.7, "$m_{t2}^{\\ell \\ell bb} \\in [100,140]$", transform = ax.transAxes, fontsize = 20, ha = 'left')

    rpcr = RegionPatch("CR$_{Wt}$", color = '#ea4335', alpha = 0.8)
    # x - axis is HT2Ratio
    # y - axis is dRll
    rpcr.x = 0.6
    rpcr.y = 0.0
    rpcr.width = 0.2
    rpcr.height = 5

    rpvr = RegionPatch("VR$_{Wt} \\downarrow$", color = 'w', alpha = 1., hatches = 'o')
    #rpvr = RegionPatch("VR$_{Wt}$", color = '#117bf3', alpha = 0.3, hatches = '//')
    rpvr.x = 0.8
    rpvr.y = 0.0
    rpvr.width = 0.2
    rpvr.height = 1.5

    rpsr = RegionPatch("$hh$", color = "#28f72d", alpha = 0.5)
    rpsr.x = 0.8
    rpsr.y = 0
    rpsr.width = 0.2
    rpsr.height = 0.9

    fig = plt.figure()
    ax = fig.add_subplot(111) #, aspect = 'equal')
    ax.set_ylim(0, 5)
    ax.set_xlim(0, 1)
    ax.grid(color = 'k', linestyle = '--', linewidth = 2, alpha = 0.8, zorder = 0)

    ax.set_xticks([0.0,0.6,0.8,1.0])
    ax.set_yticks([0.0,0.9,1.5])
    ax.set_xlabel("H$_{T2,R}$", horizontalalignment = 'right', x = 1, fontsize = 20)
    ax.set_ylabel("$\\Delta R_{\\ell \\ell}$", horizontalalignment = 'right', y = 1, fontsize = 20)

    regions = [rpcr, rpvr, rpsr]
    for ireg, reg in enumerate(regions) :
        rect = reg.as_rectangle()

        ax.add_patch( rect )

        ax.add_artist(rect)
        rx, ry = rect.get_xy()
        cx = rx + rect.get_width()/2.0
        cy = ry + rect.get_height()/2.0
        if "VR" in reg.name :
            cy += 0.9
        ax.annotate(reg.name, (cx, cy), color = 'k', weight = 'bold',
            fontsize = 20, ha = 'center', va = 'center', zorder = 10000)

    ax.text(0.05, 0.9, "$Wt$ Regions", weight = 'bold', transform = ax.transAxes, fontsize = 25, ha = 'left')
    ax.text(0.05, 0.8, "$m_{bb} > 140$ GeV", transform = ax.transAxes, fontsize = 20, ha = 'left')
    
    



    #rect = rp.as_rectangle()
    #ax.add_patch( rect )
    fig.savefig('wt_regions.pdf', dpi = 200, bbox_inches = 'tight')
    fig.show()
    plt.show()

    


if __name__ == "__main__" :
    main()
